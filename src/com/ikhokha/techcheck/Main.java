package com.ikhokha.techcheck;

import java.io.File;
import java.io.IOException;
import java.util.*;

//Mthobisi Khoza
//Full Stack
public class Main {

	public static void main(String[] args) throws InterruptedException, IOException {
		
		Map<String, Integer> totalResults = new HashMap<>();
				
		File docPath = new File("docs");
		File[] commentFiles = docPath.listFiles((d, n) -> n.endsWith(".txt"));
		List<CommentAnalyzer> threads = new ArrayList<CommentAnalyzer>();
		CommentAnalyzer commentAnalyzer = null;

		for (File commentFile : commentFiles) {
			threads.add(new CommentAnalyzer(commentFile));

			commentAnalyzer = new CommentAnalyzer(commentFile);
		}
			for (CommentAnalyzer t : threads){
			t.start();
			}

		ArrayList<String> allLines = new ArrayList<String>();

		//Adding all file texts lines in one array
		for (CommentAnalyzer t : threads) {
			t.join();
			if(t.getLines() != null){
				allLines.addAll((t.getLines()));
			}
		}

		//Getting the final report of the metrics to print on the console
		Map<String, Integer> fileResults = commentAnalyzer.analyze(allLines);
		addReportResults(fileResults, totalResults);
		System.out.println("iKhokha Metrics\n===============");
		totalResults.forEach((k,v) -> System.out.println(k + " : " + v));
	}
	
	/**
	 * This method adds the result counts from a source map to the target map 
	 * @param source the source map
	 * @param target the target map
	 */
	private static void addReportResults(Map<String, Integer> source, Map<String, Integer> target) {

		for (Map.Entry<String, Integer> entry : source.entrySet()) {
			target.put(entry.getKey(), entry.getValue());
		}
		
	}

}
