package com.ikhokha.techcheck;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CommentAnalyzer extends Thread{
	
	private File file;
	int noOfLines = 0;
	private  Map<String, Integer> resultsMap = new HashMap<>();
	
	public CommentAnalyzer(File file) {
		this.file = file;
	}

	public Map<String, Integer> analyze(ArrayList<String> textLines) {
		for(String g : textLines){
			dataAnalytic(g);
		}
		return resultsMap;
	}
	
	/**
	 * This method increments a counter by 1 for a match type on the countMap. Uninitialized keys will be set to 1
	 * @param countMap the map that keeps track of counts
	 * @param key the key for the value to increment
	 */
	private void incOccurrence(Map<String, Integer> countMap, String key) {
		
		countMap.putIfAbsent(key, 0);
		countMap.put(key, countMap.get(key) + 1);
	}

	@Override
	public void run() {

	}

	public ArrayList<String> getLines() throws IOException {

		BufferedReader reader = new BufferedReader(new FileReader(file));

		String line;
		ArrayList<String> lineArr = new ArrayList<>();

		while((line = reader.readLine()) != null)
		{
			lineArr.add(line);
			noOfLines++;

		}
		reader.close();
		return lineArr;
	}

	private void dataAnalytic(String line){
		if (line.length() < 15) {

			incOccurrence(resultsMap, "SHORTER_THAN_15");
		}
		if (line.contains("Mover")) {

			incOccurrence(resultsMap, "MOVER_MENTIONS");

		} if (line.contains("Shaker")) {

			incOccurrence(resultsMap, "SHAKER_MENTIONS");
		}
		if (line.contains("?")) {

			incOccurrence(resultsMap, "NUMBER_OF_QUESTIONS");
		}
		if (line.contains("http")) {

			incOccurrence(resultsMap, "TOTAL_SPAM");
		}
	}


}
